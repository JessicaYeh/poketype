var searchOpen = false;

$( document ).on( "pagecreate", "#main-activity", function( event ) {
    // Accept URL queries for Pokemon names
    var urlQuery = getURLParameter();
    if (urlQuery) {
        loadPokemon(urlQuery);
    }

    $("#search-icon").click(function() {
        showSearch(true);
    });

    $(".search .ui-input-clear").click(function() {
        showSearch(false);
    });

    $( "#search-autocomplete" ).on( "filterablebeforefilter", function ( e, data ) {
        var $ul = $( this ),
            $input = $( data.input ),
            value = $input.val(),
            html = "";
        $ul.html( "" );
        if ( value && value.length >= 1 ) {
            $ul.html( "<li><div class='ui-loader'><span class='ui-icon ui-icon-loading'></span></div></li>" );
            $ul.listview( "refresh" );
            $.ajax({
                url: "/api/pokemon/suggest/" + $input.val(),
                success: function(json) {
                    json = JSON.parse(json);
                    for (var i = 0; i < json.length; i++) {
                        html += '<li value="' + json[i].id + '" class="pokemon-suggestion"><a href="#"><img src="/img/pokemon' + ("00" + json[i].id).slice(-3) + '.png" class="ui-li-icon" alt="Icon">' + json[i].name + '</a></li>';
                    }
                    $ul.html( html );
                    $ul.listview( "refresh" );
                    $ul.trigger( "updatelayout");
                }
            });
        }
    });

    $(document).on('click', '.pokemon-suggestion', function() {
        clearSearch();
        loadPokemon(Number($(this).val()));
    });

    // Check if enter is pressed in search field
    $(".search input").keypress(function(e) {
        if (e.which === 13) {
            var query = $(this).val();
            clearSearch();
            loadPokemon(query);
        }
    });

    // Check if ESC is pressed in search field
    $(".search input").keyup(function(e) {
        if (e.keyCode === 27) {
            clearSearch();
        }
    });

    $(document).keyup(function(e) {
        if (!searchOpen && e.keyCode >= 65 && e.keyCode <= 90) {
            showSearch(true);
            $(".search input").val(String.fromCharCode(e.keyCode));
        }
    });
});

$(document).ready(function() {
    fixContentHeight();
});

function showSearch(show) {
    if (show) {
        $(".search").show();
        $(".search input").focus();
        $("[data-role='header'] h1").attr('visibility', 'hidden');
        $("#search-icon").hide();
        searchOpen = true;
    }
    else {
        $(".search").hide();
        $("[data-role='header'] h1").attr('visibility', 'visible');
        $("#search-icon").show();
        searchOpen = false;
    }
}

function clearSearch() {
    $(".search input").val("");
    $("#search-autocomplete").html( "" );
    $("#search-autocomplete").listview( "refresh" );
    showSearch(false);
}

// Generate the HTML for a type effectiveness item
function createTypeEffectivenessItem(type, effectiveness) {
    return '<li><img src="/img/' + type.toLowerCase() + '.png" class="ui-li-icon" alt="Icon">' + ucfirst(type) + ' <span class="ui-li-count">' + effectiveness + '%</span></li>';
}

function loadPokemon(pokemon) {
    var searchType;
    var url = "/api/pokemon/";
    if (typeof(pokemon) === "number") {
        url += "id/" + pokemon;
        searchType = "number";
    }
    else {
        url += pokemon;
        searchType = "string";
    }
    $.ajax({
        beforeSend: function() {
            $("#pokemon-info").hide();
            $("#type-effectiveness").hide();
            $.mobile.loading('show');
        },
        url: url,
        success: function(json) {
            json = JSON.parse(json);
            if (jQuery.isEmptyObject(json)) {
                if (searchType === "number") {
                    alert("There was no Pokémon found with an id of " + pokemon);
                }
                else {
                    alert("There was no Pokémon found whose name starts with '" + pokemon + "'");
                }

                return;
            }
            $("#help").hide();
            $("#amazon-appstore").hide();
            loadPokemonBasicInfo(json);
            loadTypeEffectiveness(json);

            // Refresh the listview to style it
            $('ul').listview('refresh');
        },
        complete: function() {
            // Show Pokemon info
            if (!$("#help").is(':visible')) {
                $("#pokemon-info").show();
                $("#type-effectiveness").show();
                $("#type-effectiveness").scrollTop(0);
            }

            $.mobile.loading('hide');
        }
    });
}

function loadPokemonBasicInfo(json) {
    // Set name
    $("#pokemon-name").html(json.name);
    // Set types, remove all classes except 'type' and add appropriate class for color
    $("#pokemon-type1").html(ucfirst(json.type1));
    $("#pokemon-type1").attr('class', 'type ' + json.type1);
    if (json.type2) {
        $("#pokemon-type2").html(ucfirst(json.type2));
        $("#pokemon-type2").attr('class', 'type ' + json.type2);
        $("#pokemon-type2").show();
    }
    else {
        $("#pokemon-type2").hide();
    }
    // Set image
    $("#pokemon-image img").attr('src', json.image);
}

function loadTypeEffectiveness(json) {
    // Set weaknesses
    var w = json.weaknesses;
    var weaknesses = $("#weaknesses");
    weaknesses.html("");
    for (var i = 0; i < w.length; i++) {
        weaknesses.append(createTypeEffectivenessItem(w[i].attack, w[i].effectiveness));
    }
    // Set normal damage
    var n = json.normal;
    var normal = $("#normal-damage");
    normal.html("");
    for (var i = 0; i < n.length; i++) {
        normal.append(createTypeEffectivenessItem(n[i].attack, n[i].effectiveness));
    }
    // Set resistances
    var r = json.resistances;
    var resistances = $("#resistances");
    resistances.html("");
    for (var i = 0; i < r.length; i++) {
        resistances.append(createTypeEffectivenessItem(r[i].attack, r[i].effectiveness));
    }
    // Expand weaknesses and collapse others
    $("#weaknesses-collapsible").collapsible("expand");
    $("#normal-collapsible").collapsible("collapse");
    $("#resistances-collapsible").collapsible("collapse");
}

// Make the content fill 100% of the screen
function fixContentHeight() {
    var screen = $.mobile.getScreenHeight();
    var header = $(".ui-header").hasClass("ui-header-fixed") ? $(".ui-header").outerHeight()  - 1 : $(".ui-header").outerHeight();
    var footer = $(".ui-footer").hasClass("ui-footer-fixed") ? $(".ui-footer").outerHeight() - 1 : $(".ui-footer").outerHeight();
    /* content div has padding of 1em = 16px (32px top+bottom). This step
       can be skipped by subtracting 32px from content var directly. */
    var contentCurrent = $(".ui-content").outerHeight() - $(".ui-content").height();
    var content = screen - header - footer - contentCurrent;
    $(".ui-content").height(content);
    // Adjust size of Pokemon info boxes
    $("#pokemon-info").height(content*0.42);
    $("#help").height(content*0.42);
    $("#pokemon-image").height($("#pokemon-info").outerHeight() - $("#pokemon-name-types").outerHeight() + 2);
    $("#type-effectiveness").height(content*0.42);
}

function ucfirst(string) {
    if (string) {
        return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
    }
    return false;
}

function getURLParameter(param) {
    // Get the URL path (not including domain)
    var path = window.location.pathname;
    // Make sure the path ends with a /
    if (path.charAt(-1) !== "/") {
        path += "/";
    }
    // If a param is specified, find it and trim path to it
    if (param) {
        // Find the parameter, exit with false if not found
        var paramIndex = path.indexOf("/" + param + "/");
        if (paramIndex === -1) {
            return false;
        }
        // Size of the param + slashes
        var paramLength = param.length + 2;
        // Trim the path to after where the param was found
        path = path.substring(paramIndex + paramLength);
    }
    // Get the value of the parameter
    if (path.indexOf("/") === 0) {
        path = path.substring(1);
    }
    var value = path.substring(0, path.indexOf("/"));
    
    // If the value is a number type, return the number type
    return isNaN(value) ? value : Number(value);
}